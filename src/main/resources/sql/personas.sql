CREATE DATABASE ejemplo;

USE ejemplo;

CREATE TABLE persona
(
	id INT UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT 
	,nombre VARCHAR(60)
	,apellidos VARCHAR(60)
);

INSERT INTO persona(nombre,apellidos) VALUES ('Pablo','Mora');
INSERT INTO persona(nombre,apellidos) VALUES ('Kevin','Bolivar');

