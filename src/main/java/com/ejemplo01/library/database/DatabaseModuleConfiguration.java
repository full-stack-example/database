package com.ejemplo01.library.database;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = "com.ejemplo01.library.database")
public class DatabaseModuleConfiguration {
}