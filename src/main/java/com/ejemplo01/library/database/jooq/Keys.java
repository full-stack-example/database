/*
 * This file is generated by jOOQ.
 */
package com.ejemplo01.library.database.jooq;


import com.ejemplo01.library.database.jooq.tables.Persona;
import com.ejemplo01.library.database.jooq.tables.records.PersonaRecord;

import javax.annotation.Generated;

import org.jooq.Identity;
import org.jooq.UniqueKey;
import org.jooq.impl.Internal;
import org.jooq.types.UInteger;


/**
 * A class modelling foreign key relationships and constraints of tables of 
 * the <code>ejemplo</code> schema.
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.12.3"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class Keys {

    // -------------------------------------------------------------------------
    // IDENTITY definitions
    // -------------------------------------------------------------------------

    public static final Identity<PersonaRecord, UInteger> IDENTITY_PERSONA = Identities0.IDENTITY_PERSONA;

    // -------------------------------------------------------------------------
    // UNIQUE and PRIMARY KEY definitions
    // -------------------------------------------------------------------------

    public static final UniqueKey<PersonaRecord> KEY_PERSONA_PRIMARY = UniqueKeys0.KEY_PERSONA_PRIMARY;

    // -------------------------------------------------------------------------
    // FOREIGN KEY definitions
    // -------------------------------------------------------------------------


    // -------------------------------------------------------------------------
    // [#1459] distribute members to avoid static initialisers > 64kb
    // -------------------------------------------------------------------------

    private static class Identities0 {
        public static Identity<PersonaRecord, UInteger> IDENTITY_PERSONA = Internal.createIdentity(Persona.PERSONA, Persona.PERSONA.ID);
    }

    private static class UniqueKeys0 {
        public static final UniqueKey<PersonaRecord> KEY_PERSONA_PRIMARY = Internal.createUniqueKey(Persona.PERSONA, "KEY_persona_PRIMARY", Persona.PERSONA.ID);
    }
}
