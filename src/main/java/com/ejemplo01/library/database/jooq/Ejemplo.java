/*
 * This file is generated by jOOQ.
 */
package com.ejemplo01.library.database.jooq;


import com.ejemplo01.library.database.jooq.tables.Persona;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.annotation.Generated;

import org.jooq.Catalog;
import org.jooq.Table;
import org.jooq.impl.SchemaImpl;


/**
 * This class is generated by jOOQ.
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.12.3"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class Ejemplo extends SchemaImpl {

    private static final long serialVersionUID = -1360186793;

    /**
     * The reference instance of <code>ejemplo</code>
     */
    public static final Ejemplo EJEMPLO = new Ejemplo();

    /**
     * The table <code>ejemplo.persona</code>.
     */
    public final Persona PERSONA = com.ejemplo01.library.database.jooq.tables.Persona.PERSONA;

    /**
     * No further instances allowed
     */
    private Ejemplo() {
        super("ejemplo", null);
    }


    @Override
    public Catalog getCatalog() {
        return DefaultCatalog.DEFAULT_CATALOG;
    }

    @Override
    public final List<Table<?>> getTables() {
        List result = new ArrayList();
        result.addAll(getTables0());
        return result;
    }

    private final List<Table<?>> getTables0() {
        return Arrays.<Table<?>>asList(
            Persona.PERSONA);
    }
}
